# Umbraco .NET Test

This test should not take too long. Please keep track of time and as you send us your project, let us know an approximate estimate, how long did it take you to complete it. If you have any questions please don't hesitate to ask.

# Goal

Working Umbraco 8 Visual Studio Project in your Bitbucket repository (please provide us with a link and make sure it has public access). Make sure all of the necessary files are included, for this project to work. The website must look like the given frontend files (layout-wise, not content-wise), with editable content in Umbraco (for all fields except the ones that are being pulled from the API).

Use the appropriate Umbraco tools (Document types, fields, etc.)

# Files included

You were given the frontend files with built HTML, CSS, and js (build.zip in this repository). You were also given the source files for the built frontend files (You can use them to speed up your conversion to the templates in Umbraco).

# We are looking for:

1. Maintainable and testable code
2. Unit/acceptance/feature tests if deemed necessary (please add at least 2 tests, even if they are trivial)
3. Separation of concern
4. Use common practices
5. Fallback behavior if third party services are unavailable (the API)

# API Access & Instructions

Pull your items from this source:

```
GET https://www.jsonstore.io/3f3b001398973ab65953b73e2da267bd9a7c5400483ff8a73c75c0e1fc76075c
```

Make sure you return only products that are in the "display" category, filter out the "filler" ones. Reverse the order of the products being displayed (do this in the backend). Trim the description text, so it fits nicely on the cards in the frontend.

The index page should fetch all data from Umbraco, the cards on the api.html page should be filled with data from the API with the rules mentioned above.

Good luck :)
