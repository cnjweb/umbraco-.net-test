{
	"products": [
		{
			"id": 1,
			"image": "https://picsum.photos/id/18/500/500",
			"name": "Grass field",
			"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
			"category": "display"
		},
		{
			"id": 33,
			"image": "https://picsum.photos/id/16/500/500",
			"name": "Stranded on the sea",
			"description": "Nec sagittis aliquam malesuada bibendum arcu vitae elementum. In nibh mauris cursus mattis molestie. ",
			"category": "display"
		},
		{
			"id": 6,
			"image": "https://picsum.photos/id/6/500/500",
			"name": "Laptop on desk",
			"description": "Convallis aenean et tortor at risus viverra adipiscing at. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae. Amet nulla facilisi morbi tempus iaculis.",
			"category": "display"
		},
		{
			"id": 2,
			"image": "https://picsum.photos/id/10/500/500",
			"name": "Calm forest",
			"description": "Urna molestie at elementum eu facilisis sed odio morbi quis.",
			"category": "display"
		},
		{
			"id": 4,
			"image": "https://picsum.photos/id/1/500/500",
			"name": "Filler 1",
			"description": "This item must be hidden.",
			"category": "filler"
		},
		{
			"id": 82,
			"image": "https://picsum.photos/id/1/500/500",
			"name": "Filler 2",
			"description": "This item must be hidden.",
			"category": "filler"
		},
		{
			"id": 7,
			"image": "https://picsum.photos/id/1/500/500",
			"name": "Filler 3",
			"description": "This item must be hidden.",
			"category": "filler"
		}
	]
}
